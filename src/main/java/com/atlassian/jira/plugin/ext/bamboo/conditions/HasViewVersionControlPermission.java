package com.atlassian.jira.plugin.ext.bamboo.conditions;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.web.Condition;
import com.atlassian.studio.haup.api.UserApplicationAccessService;

import java.util.Map;

import static com.atlassian.jira.component.ComponentAccessor.getOSGiComponentInstanceOfType;
import static com.atlassian.jira.permission.ProjectPermissions.VIEW_DEV_TOOLS;
import static com.atlassian.studio.haup.api.SupportedApplication.BAMBOO;
import static com.google.common.base.Preconditions.checkNotNull;

public class HasViewVersionControlPermission implements Condition {
    private final FeatureManager featureManager;
    private final JiraAuthenticationContext authContext;
    private final PermissionManager permissionManager;

    public HasViewVersionControlPermission(
            @ComponentImport final FeatureManager featureManager,
            @ComponentImport final JiraAuthenticationContext authContext,
            @ComponentImport final PermissionManager permissionManager) {
        this.authContext = checkNotNull(authContext);
        this.featureManager = checkNotNull(featureManager);
        this.permissionManager = checkNotNull(permissionManager);
    }

    @Override
    public void init(final Map<String, String> configParams) {
    }

    @Override
    public boolean shouldDisplay(final Map<String, Object> context) {
        final ApplicationUser user = authContext.getLoggedInUser();

        if (user == null) {
            return false;
        }

        if (featureManager.isOnDemand()) {
            final UserApplicationAccessService userApplicationAccessService =
                    getOSGiComponentInstanceOfType(UserApplicationAccessService.class);
            if (!userApplicationAccessService.hasAccess(user.getName(), BAMBOO)) {
                return false;
            }
        }

        if (context.containsKey("issue")) {
            final Issue issue = (Issue) context.get("issue");
            return permissionManager.hasPermission(VIEW_DEV_TOOLS, issue, user);
        }

        if (context.containsKey("project")) {
            final Project project = (Project) context.get("project");
            return permissionManager.hasPermission(VIEW_DEV_TOOLS, project, user);
        }

        return false;
    }
}
