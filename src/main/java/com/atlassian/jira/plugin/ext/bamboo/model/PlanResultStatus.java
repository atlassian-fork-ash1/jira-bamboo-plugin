package com.atlassian.jira.plugin.ext.bamboo.model;

import org.json.JSONException;
import org.json.JSONObject;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Represents the Status of a particular PlanResult on a Bamboo server
 */
public class PlanResultStatus {
    private final PlanResultKey planResultKey;
    private final BuildState buildState;
    private final LifeCycleState lifeCycleState;
    private final boolean recoverable;

    public static PlanResultStatus fromJsonObject(PlanResultKey planResultKey, JSONObject jsonObject) throws JSONException {
        final String state = jsonObject.getString("state");
        final String lifecycle = jsonObject.getString("lifeCycleState");

        final BuildState buildState = BuildState.getInstance(state);
        final LifeCycleState lifeCycleState = LifeCycleState.getInstance(lifecycle);

        return new PlanResultStatus(planResultKey, buildState, lifeCycleState, true);
    }

    public PlanResultStatus(final PlanResultKey planResultKey, final BuildState buildState, final LifeCycleState lifeCycleState, final boolean recoverable) {
        this.planResultKey = planResultKey;
        this.buildState = buildState;
        this.lifeCycleState = lifeCycleState;
        this.recoverable = recoverable;
    }

    /**
     * @return resultKey
     */
    @Nonnull
    public PlanResultKey getPlanResultKey() {
        return planResultKey;
    }

    /**
     * @return buildState
     */
    @Nullable
    public BuildState getBuildState() {
        return buildState;
    }

    /**
     * @return lifeCycleState
     */
    @Nullable
    public LifeCycleState getLifeCycleState() {
        return lifeCycleState;
    }

    /**
     * @return is a valid {@link com.atlassian.jira.plugin.ext.bamboo.model.PlanResultStatus}
     */
    public boolean isValid() {
        return lifeCycleState != null && buildState != null;
    }

    /**
     * Was the status of the plan in a recoverable error state?
     *
     * @return recoverable
     */
    public boolean isRecoverable() {
        return recoverable;
    }

    @Override
    public String toString() {
        return "PlanStatus{" +
                "planResultKey=" + planResultKey +
                ", buildState=" + buildState +
                ", lifeCycleState=" + lifeCycleState +
                ", recoverable=" + recoverable +
                '}';
    }
}
