package com.atlassian.jira.plugin.ext.bamboo.service;

import io.atlassian.fugue.Either;

import javax.annotation.Nonnull;

/**
 * Business logic relating to versions of Jira projects.
 */
public interface ProjectVersionService {
    /**
     * Returns the number of unresolved issues in the given version.
     *
     * @param projectId the project to which the version belongs
     * @param versionId the version for which to count the unresolved issues
     * @return either a translated error message or the count
     */
    @Nonnull
    Either<String, Integer> getUnresolvedIssueCount(long projectId, long versionId);
}
