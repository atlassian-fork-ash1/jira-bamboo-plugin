package com.atlassian.jira.plugin.ext.bamboo.gadgets;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.gadgets.GadgetSpecProvider;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.google.common.collect.Iterables;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.annotation.concurrent.ThreadSafe;
import java.net.URI;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import static com.google.common.collect.Maps.newLinkedHashMap;
import static java.util.stream.Collectors.toList;

@ThreadSafe
@ExportAsService(GadgetSpecProvider.class)
@Component
public class BambooGadgetSpecProvider implements GadgetSpecProvider {
    @SuppressWarnings("unused")
    private static final Logger log = Logger.getLogger(BambooGadgetSpecProvider.class);

    private final Map<ApplicationId, Set<URI>> bambooGadgets = newLinkedHashMap();

    /**
     * Get all gadget URIs for all Bamboo application links.
     */
    public Iterable<URI> entries() {
        return bambooGadgets.values().stream().flatMap(Collection::stream).collect(toList());
    }

    public boolean contains(final URI uri) {
        return Iterables.contains(entries(), uri);
    }

    /**
     * Add the specified gadget URI for the specified application id
     *
     * @param appId     the application id
     * @param gadgetUri the gadget URI
     */
    public void addBambooGadget(final ApplicationId appId, final URI gadgetUri) {
        log.debug(String.format("Adding Bamboo gadget %s hosted on %s", gadgetUri, appId));

        if (!bambooGadgets.containsKey(appId)) {
            bambooGadgets.put(appId, new CopyOnWriteArraySet<>());
        }

        bambooGadgets.get(appId).add(gadgetUri);
    }

    /**
     * Remove all Bamboo gadgets for the specified application id
     */
    public void removeBambooGadgets(ApplicationId appId) {
        bambooGadgets.remove(appId);
    }

    /**
     * Migrate an application id's Bamboo gadgets to be registered to a different application id.
     */
    public void migrateBambooGadgets(ApplicationId oldId, ApplicationId newId) {
        Set<URI> gadgets = bambooGadgets.remove(oldId);
        if (gadgets != null) {
            bambooGadgets.put(newId, gadgets);
        }
    }

    public void clear() {
        bambooGadgets.clear();
    }
}
