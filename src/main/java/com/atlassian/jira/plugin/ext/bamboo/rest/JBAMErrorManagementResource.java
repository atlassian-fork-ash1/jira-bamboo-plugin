package com.atlassian.jira.plugin.ext.bamboo.rest;

import com.atlassian.jira.plugin.ext.bamboo.service.ReleaseErrorReportingService;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.base.Preconditions;
import org.apache.log4j.Logger;

import javax.annotation.Nonnull;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.google.common.base.Preconditions.checkNotNull;

@Path("/errors")
public class JBAMErrorManagementResource {
    private static final Logger log = Logger.getLogger(JBAMErrorManagementResource.class);

    private final ReleaseErrorReportingService releaseErrorReportingService;
    private final VersionManager versionManager;

    public JBAMErrorManagementResource(
            final ReleaseErrorReportingService releaseErrorReportingService,
            @ComponentImport final VersionManager versionManager) {
        this.releaseErrorReportingService = checkNotNull(releaseErrorReportingService);
        this.versionManager = checkNotNull(versionManager);
    }

    @DELETE
    @Path("/{versionId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteErrors(@Nonnull @PathParam("versionId") final Long versionId) {
        try {
            Preconditions.checkNotNull(versionId, "versionId");

            final Version version = versionManager.getVersion(versionId);
            if (version == null) {
                log.warn("Could not clear release errors for version, no version with the id '" + versionId + "' could be found");
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            final Project project = version.getProject();
            releaseErrorReportingService.clearErrors(project.getKey(), version.getId());
            return Response.ok().build();
        } catch (RuntimeException e) {
            log.warn("Could not clear release errors for version " + versionId + " : " + e.getMessage());
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
