package it.com.atlassian.jira.plugin.ext.bamboo.ctk;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.webdriver.testing.rule.WebDriverScreenshotRule;
import com.google.common.collect.ImmutableList;
import it.com.atlassian.jira.plugin.ext.bamboo.JiraProductInstance;
import it.com.atlassian.jira.plugin.ext.bamboo.pageobjects.InlineDialog;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.atlassian.fusion.test.FusionResource.Bamboo;
import static com.atlassian.jira.plugin.ext.bamboo.applinks.BambooApplicationLinkManagerImpl.ENFORCE_JBAM_FEATURE;
import static com.atlassian.pageobjects.TestedProductFactory.create;
import static com.atlassian.webdriver.utils.Check.elementExists;
import static com.atlassian.webdriver.utils.Check.elementIsVisible;
import static it.com.atlassian.jira.plugin.ext.bamboo.ctk.CtkServer.start;
import static it.com.atlassian.jira.plugin.ext.bamboo.ctk.CtkServer.stop;
import static it.com.atlassian.jira.plugin.ext.bamboo.utils.AppLinkTypes.BAMBOO;
import static it.com.atlassian.jira.plugin.ext.bamboo.utils.PrivacyPolicyAcknowledgeControl.acknowledgePrivacyPolicy;
import static java.net.HttpURLConnection.HTTP_CREATED;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertTrue;

/**
 * A functional test that connects an already running JIRA to two Bamboo CTK
 * servers, i.e. stub HTTP servers that this test case configures to return
 * specific JSON responses to specific REST requests.
 */
@Ignore("FUSE-2470: Ignoring temporarily to make Jira breakit BP build green. Tested manually that it works. Also works locally. But consistently fails when run on Bamboo.")
public class JiraBambooPluginCtkTest {
    @Rule
    public WebDriverScreenshotRule screenshotRule = new WebDriverScreenshotRule();

    // How long to wait after asking testkit to create an app link
    private static final long APP_LINK_WAIT_TIME_MILLIS = TimeUnit.SECONDS.toMillis(30);

    private static final By BUILDS_TAB_PANEL = By.id("bamboo-build-results-tabpanel");
    private static final By DEPLOYMENT_PANEL = By.id("bamboo-deployment-projects-right");

    // The name of this Fusion dark feature
    private static final String DEV_STATUS_3LO_DISABLED = "jira.devstatus.oauth.3lo.disabled";
    private static final String ISSUE_KEY = "ONE-1";
    private static final String PROJECT_KEY = "ONE";

    private static final String DEPLOYMENTS_JSON1 = "{\"deploymentProjects\":[{"
            + "\"name\":\"Invisible\","
            + "\"id\":35913729,"
            + "\"url\":\"http://novigrad:8085/bamboo/deploy/viewDeploymentProjectEnvironments.action?id=35913729\","
            + "\"environmentCount\":2,"
            + "\"upToDateEnvironmentCount\":0}]}";

    private static final String DEPLOYMENTS_JSON2 = "{\"deploymentProjects\":[{"
            + "\"name\":\"You can see me\","
            + "\"id\":35913729,"
            + "\"url\":\"http://novigrad:8085/bamboo/deploy/viewDeploymentProjectEnvironments.action?id=35913729\","
            + "\"environmentCount\":2,"
            + "\"upToDateEnvironmentCount\":0}]}";

    // Fixture
    private Backdoor backdoor;
    private BambooCtkServer bambooWithDevStatus;
    private BambooCtkServer bambooWithNoDevStatus;
    private JiraTestedProduct jira;
    private WebDriver webDriver;

    @Before
    public void setUp() {
        jira = create(JiraTestedProduct.class, JiraProductInstance.INSTANCE, null);
        backdoor = jira.backdoor();
        webDriver = jira.getTester().getDriver();

        bambooWithDevStatus = BambooCtkServer.getLocalInstance(8990);
        bambooWithNoDevStatus = BambooCtkServer.getLocalInstance(8991);

        start(bambooWithDevStatus, bambooWithNoDevStatus);

        bambooWithDevStatus.setCapabilities(Bamboo.CAPABILITIES);
        bambooWithDevStatus.setIssueDeploymentStatus(ISSUE_KEY, DEPLOYMENTS_JSON1);
        bambooWithDevStatus.set2LoEnabled();

        bambooWithNoDevStatus.setCapabilities(Bamboo.CAPABILITIES_NO_DEV_STATUS_SUMMARY);
        bambooWithNoDevStatus.setIssueDeploymentStatus(ISSUE_KEY, DEPLOYMENTS_JSON2);
    }

    @After
    public void tearDown() {
        stop(bambooWithDevStatus, bambooWithNoDevStatus);
    }

    /**
     * Creates an app link from the Jira under test to the given Bamboo server.
     *
     * @param name the name of the app link
     * @param url  the URL of the Bamboo instance
     */
    private void createAppLinkToBamboo(final String name, final String url) {
        try {
            final String xmlResponse =
                    backdoor.getTestkit().applicationLink().addApplicationLink(BAMBOO.get(), name, url);
            assertThat(xmlResponse, containsString("<status-code>" + HTTP_CREATED + "</status-code>"));
            // Why this wait? Something to do with the nav link cache, perhaps?
            Thread.sleep(APP_LINK_WAIT_TIME_MILLIS);
        } catch (final InterruptedException | JSONException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void buildTabAndDeploymentPanelShouldBeVisibleWhenFusionCanNotDisplayBuildAndDeployInfo() throws Exception {
        backdoor.restoreBlankInstance();
        backdoor.project().addProject("ONE", PROJECT_KEY, "admin");
        backdoor.issues().createIssue(PROJECT_KEY, "Konstantynopolitanczykowna");
        backdoor.darkFeatures().enableForSite(DEV_STATUS_3LO_DISABLED);
        backdoor.systemProperties().setProperty(DEV_STATUS_3LO_DISABLED, String.valueOf(true));

        createAppLinkToBamboo("Bamboo One", bambooWithDevStatus.getBaseUrl());

        // If we enable the dark feature, the deployment panel should be visible
        jira.gotoLoginPage().loginAndGoToHome("admin", "admin");
        acknowledgePrivacyPolicy(jira);

        backdoor.darkFeatures().enableForSite(ENFORCE_JBAM_FEATURE);

        goToViewIssueAndDismissDialogs(ISSUE_KEY);

        assertTrue("build tab should be visible", elementExists(BUILDS_TAB_PANEL, webDriver));
        assertTrue("deployment panel should be visible", elementIsVisible(DEPLOYMENT_PANEL, webDriver));
        assertTrue("project from Bamboo One should be shown", getDeploymentPanelText().contains("Invisible"));
    }

    @Test
    public void buildTabAndDeploymentPanelShouldBeVisibleWhenBambooIsNotDevstatusCapable() throws Exception {
        backdoor.restoreBlankInstance();
        backdoor.project().addProject("ONE", PROJECT_KEY, "admin");
        backdoor.issues().createIssue(PROJECT_KEY, "Konstantynopolitanczykowna");
        createAppLinkToBamboo("Bamboo Two", bambooWithNoDevStatus.getBaseUrl());

        jira.gotoLoginPage().loginAndGoToHome("admin", "admin");
        acknowledgePrivacyPolicy(jira);
        goToViewIssueAndDismissDialogs(ISSUE_KEY);

        assertTrue("build tab should be visible", elementExists(BUILDS_TAB_PANEL, webDriver));
        assertTrue("deployment panel should be visible", elementIsVisible(DEPLOYMENT_PANEL, webDriver));
        assertTrue("project from Bamboo Two should be shown", getDeploymentPanelText().contains("You can see me"));
    }

    private void goToViewIssueAndDismissDialogs(@Nonnull final String issueKey) {
        jira.goToViewIssue(issueKey);

        for (InlineDialog dialog : getDialogsToDismiss()) {
            dialog.dismissDialogIfVisible();
        }
    }

    private List<InlineDialog> getDialogsToDismiss() {
        InlineDialog aitDialog = bindToDialogWithTypeSelector(".dev-summary-automatic-transitions");
        return ImmutableList.of(aitDialog);
    }

    private InlineDialog bindToDialogWithTypeSelector(final String dialogTypeSelector) {
        return jira.getPageBinder().bind(InlineDialog.class, dialogTypeSelector);
    }

    private String getDeploymentPanelText() {
        return jira.getTester().getDriver().findElement(DEPLOYMENT_PANEL).getText();
    }
}
