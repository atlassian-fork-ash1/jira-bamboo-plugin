package it.com.atlassian.jira.plugin.ext.bamboo.ctk;

import com.atlassian.federation.backdoor.CtkServerResourceModifier;
import com.atlassian.federation.server.Server;
import com.atlassian.pageobjects.elements.query.AbstractTimedCondition;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.atomic.AtomicReference;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilEquals;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.net.HttpURLConnection.HTTP_OK;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * A product-agnostic stub server that test cases can set up to respond to HTTP
 * requests in a given way, see {@link com.atlassian.federation.backdoor.CtkServerResourceModifier#setResource}.
 */
public class CtkServer {
    private static final Logger LOGGER = LoggerFactory.getLogger(CtkServer.class);

    // How long to wait for a connection to the  CTK server
    private static final int CONNECTION_TIMEOUT_MILLIS = (int) SECONDS.toMillis(2);

    // How long to wait for all CTK servers to change state up or down
    private static final long SERVER_STATE_TIMEOUT = SECONDS.toMillis(30);

    // How often to check whether all CTK servers have changed state
    private static final long SERVER_STATE_POLLING_INTERVAL = SECONDS.toMillis(1);

    /**
     * Starts the given servers and blocks until they are all up.
     *
     * @param servers the servers to start
     */
    public static void start(@Nonnull final CtkServer... servers) {
        for (final CtkServer server : servers) {
            server.start();
        }
        waitFor(true, servers);
    }

    /**
     * Stops the given servers and blocks until they are all down.
     *
     * @param servers the servers to stop
     */
    public static void stop(@Nonnull final CtkServer... servers) {
        for (final CtkServer server : servers) {
            server.stop();
        }
        waitFor(false, servers);
    }

    /**
     * Waits for the given CTK servers to all be either up or down, as specified by the given flag.
     *
     * @param desiredState true to wait for them to be up, false to wait for them to be down
     */
    private static void waitFor(final boolean desiredState, final CtkServer... servers) {
        final TimedCondition areAllServersUp =
                new AbstractTimedCondition(SERVER_STATE_TIMEOUT, SERVER_STATE_POLLING_INTERVAL) {
                    @Override
                    protected Boolean currentValue() {
                        boolean up = true;
                        for (final CtkServer server : servers) {
                            up = up && server.tryConnect();
                        }
                        return up;
                    }
                };

        waitUntilEquals(desiredState, areAllServersUp);
    }

    private final CtkServerResourceModifier resourceModifier;
    private final AtomicReference<Server> serverReference;
    private final Server.Config config;
    private final String baseUrl;

    public CtkServer(@Nonnull final Server.Config config) {
        this.config = checkNotNull(config);
        this.serverReference = new AtomicReference<Server>();
        this.baseUrl = "http://" + config.host() + ":" + config.port();
        this.resourceModifier = new CtkServerResourceModifier(baseUrl);
    }

    /**
     * Sets up this server to return the given content for the given resource.
     *
     * @param resource    the URL being requested
     * @param content     the content to return
     * @param contentType the content type
     */
    protected final void setResource(final String resource, final String content, final MediaType contentType) {
        resourceModifier.setResource(resource, content, contentType.toString());
    }

    /**
     * Returns this server's base URL.
     *
     * @return a URL with no trailing slash
     */
    @Nonnull
    public String getBaseUrl() {
        return baseUrl;
    }

    public boolean tryConnect() {
        final URL url;
        try {
            url = new URL(baseUrl + "/");
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }

        final HttpURLConnection httpConnection;
        try {
            httpConnection = (HttpURLConnection) url.openConnection();
            httpConnection.setConnectTimeout(CONNECTION_TIMEOUT_MILLIS);
            httpConnection.setRequestMethod("GET");
            if (httpConnection.getResponseCode() == HTTP_OK) {
                return true;
            }
            throw new RuntimeException("Server misbehaving: " + httpConnection.getResponseCode());
        } catch (final IOException e) {
            LOGGER.debug(String.format("Error connecting to '%s'", baseUrl), e);
            return false;
        }
    }

    public void start() {
        final boolean successfullySet = serverReference.compareAndSet(null, new Server());

        if (!successfullySet) {
            throw new IllegalStateException("Cannot start while already started.");
        }
        serverReference.get().start(config);
    }

    public void stop() {
        final Server server = serverReference.getAndSet(null);
        if (server != null) {
            server.stop();
        }
    }
}
