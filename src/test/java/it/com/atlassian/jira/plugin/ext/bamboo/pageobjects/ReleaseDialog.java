package it.com.atlassian.jira.plugin.ext.bamboo.pageobjects;

import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.CheckboxElement;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.Options;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.SelectElement;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.google.common.base.Function;
import it.com.atlassian.jira.plugin.ext.bamboo.PageElementUtils;
import org.hamcrest.Matcher;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.atlassian.pageobjects.elements.query.Conditions.and;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;

/**
 * Page object for a dialog on the release management tab, to release versions in Bamboo.
 */
public class ReleaseDialog {
    // TODO better checking is it's open or not

    @ElementBy(id = "release-form")
    private PageElement releaseForm;

    @ElementBy(cssSelector = ".aui-icon.icon-date")
    private PageElement calendarIcon;

    @Inject
    private PageElementFinder elementFinder;

    private PageElement noBuildRadioButton;
    private PageElement newBuildLink;
    private PageElement existingBuildLink;
    private PageElement existingBuildsContainer;
    private PageElement releaseButton;
    private SelectElement planSelect;

    @Init
    private void initialize() {
        newBuildLink = releaseForm.find(By.id("new-build"), TimeoutType.DIALOG_LOAD);
        existingBuildLink = releaseForm.find(By.id("existing-build"));
        planSelect = releaseForm.find(By.id("bamboo-plan"), SelectElement.class);
        existingBuildsContainer = releaseForm.find(By.id("bamboo-build-results"));
        releaseButton = releaseForm.find(By.id("release"));
        noBuildRadioButton = releaseForm.find(By.id("no-build"));
        hideGlobalWarningMessage();
    }

    /**
     * Return whether or not the release dialog is currently open in the form of a {@link TimedCondition}.
     *
     * The logic for deciding whether the dialog is open differs depending on whether or not Bamboo is connected to
     * Jira.
     *
     * @param bambooConnected true if Bamboo is connected to Jira and false otherwise
     * @return see above
     */
    public TimedCondition isOpen(boolean bambooConnected) {
        if (bambooConnected) {
            return Conditions.and(releaseForm.timed().isVisible(),
                    elementFinder.find(By.id("bamboo-plan")).timed().isVisible(),
                    elementFinder.find(By.id("bamboo-plan-stages")).timed().isVisible());
        }
        return releaseForm.timed().isVisible();
    }

    public ReleaseDialog selectNewBuild(String planKey) {
        return selectBuild(newBuildLink, planKey);
    }

    public ReleaseDialog selectExistingBuild(String planKey) {
        return selectBuild(existingBuildLink, planKey);
    }

    private ReleaseDialog selectBuild(PageElement link, String planKey) {
        link.click();
        waitUntil(Conditions.and(planSelect.timed().isPresent(), planSelect.timed().isEnabled()), (Matcher<Boolean>) is(true), Poller.by(30, SECONDS));
        final PageElement option = releaseForm.find(By.cssSelector("select option[value='" + planKey + "']"));
        Poller.waitUntilTrue(option.withTimeout(TimeoutType.DIALOG_LOAD).timed().isPresent());
        planSelect.select(Options.text(option.getText()));
        Poller.waitUntilTrue(option.timed().isSelected());
        return this;
    }

    public ReleaseDialog selectNoBuild() {
        noBuildRadioButton.click();
        return this;
    }

    /**
     * Select build number to execute for release by an existing build. Applicable only when existing build is selected
     * for the release.
     *
     * @param number number of the build to select
     * @return this dialog instance
     * @see #selectExistingBuild(String)
     */
    public ReleaseDialog selectBuildNumber(int number) {
        Poller.waitUntilTrue("Only applicable when existing build is selected", isExistingBuildSelected());
        final PageElement buildToSelect = existingBuildsContainer.find(By.id("build-result-" + number));
        assertTrue("Build with number <" + number + "> not found", buildToSelect.isPresent());
        buildToSelect.select();
        return this;
    }

    public List<PageElement> allExistingBuilds() {
        Poller.waitUntilTrue("Only applicable when existing build is selected", isExistingBuildSelected());
        return existingBuildsContainer.findAll(By.tagName("input"));
    }

    public TimedCondition isExistingBuildSelected() {
        return and(existingBuildLink.timed().isSelected(), existingBuildsContainer.timed().isVisible());
    }


    public ReleaseDialog selectStages(String... stages) {
        return doWithStages(PageElementUtils.CHECK, stages);
    }

    public ReleaseDialog unselectStages(String... stages) {
        return doWithStages(PageElementUtils.UNCHECK, stages);
    }

    public ReleaseDialog doWithStages(Function<CheckboxElement, CheckboxElement> whatToDo, String... stages) {
        if (stages.length == 0) {
            return this;
        }
        for (String stage : stages) {
            CheckboxElement element = findStageCheckbox(stage);
            if (element.isVisible()) {
                whatToDo.apply(element);
            }
        }
        return this;
    }

    public ReleaseDialog selectAllStages() {
        return doWithAllStages(PageElementUtils.CHECK);
    }

    public ReleaseDialog unselectAllStages() {
        return doWithAllStages(PageElementUtils.UNCHECK);
    }

    public ReleaseDialog doWithAllStages(Function<CheckboxElement, CheckboxElement> whatToDo) {
        for (CheckboxElement stageElement : allStages()) {
            if (stageElement.isVisible()) {
                whatToDo.apply(stageElement);
            }
        }
        return this;
    }

    public boolean hasStageCheckbox(String stage) {
        return findStageCheckboxNoException(stage) != null;
    }

    public CheckboxElement findStageCheckbox(String stage) {
        final CheckboxElement answer = findStageCheckboxNoException(stage);
        if (answer == null) {
            throw new IllegalArgumentException("No stage checkbox found for " + stage);
        }
        return answer;
    }

    private CheckboxElement findStageCheckboxNoException(String stage) {
        for (CheckboxElement stageElement : allStages()) {
            if (stageElement.getValue().equals(stage)) {
                return stageElement;
            }
        }
        return null;
    }

    /**
     * Stages container is visible while AJAX-loading stages, so we need to check for something other visibility
     */
    private Iterable<CheckboxElement> allStages() {
        PageElement stagesContainer = elementFinder.find(By.id("bamboo-plan-stages"));
        waitUntilTrue(stagesContainer.find(By.cssSelector("div.checkbox")).timed().isVisible());
        return stagesContainer.findAll(By.cssSelector("input[type=checkbox]"), CheckboxElement.class);
    }

    /**
     * Submit the release form in order to release the Jira version in play.
     *
     * Whether or not Bamboo is connected to Jira affects things like how we test if the dialog is open.
     *
     * @param bambooConnected true is Bamboo is connected to Jira and false otherwise
     * @return the release dialog page object
     */
    public ReleaseDialog submit(boolean bambooConnected) {
        releaseButton.click();
        waitUntil(isOpen(bambooConnected), (Matcher<Boolean>) is(false), Poller.by(30, SECONDS));
        return this;
    }

    /**
     * Submit the release form but don't wait for the form to disappear, since we expect errors with the
     * submission to keep the form visible.
     *
     * @return the release dialog page object
     */
    public ReleaseDialog submitWithErrors() {
        releaseButton.click();
        return this;
    }

    private void hideGlobalWarningMessage() {
        if (elementFinder.find(By.partialLinkText("hide this message")).isPresent()) {
            final PageElement pageElement = elementFinder.find(By.partialLinkText("hide this message"));
            if (pageElement != null) {
                pageElement.click();
            }
        }
    }

    /**
     * Set the release date of this release to today using the date picker.
     */
    public void setReleaseDateToToday() {
        calendarIcon.click();
        PageElement calendar = elementFinder.find(By.cssSelector(".calendar.active"));
        waitUntilTrue("calendar should be visible", calendar.timed().isVisible());

        PageElement today = calendar.find(By.cssSelector(".day.selected"));
        today.click();
        waitUntilFalse("calendar should not be visible after selecting the date", calendar.timed().isVisible());
    }

    public boolean hasReleaseDateError() {
        return elementFinder.find(
                By.id("release-form-release-date-error")
        ).timed().isPresent().by(10000l, TimeUnit.MILLISECONDS);
    }
}
