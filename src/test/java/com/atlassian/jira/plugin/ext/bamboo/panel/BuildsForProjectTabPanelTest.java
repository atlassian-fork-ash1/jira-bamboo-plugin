package com.atlassian.jira.plugin.ext.bamboo.panel;

import com.atlassian.jira.plugin.ext.bamboo.applinks.BambooApplicationLinkManager;
import com.atlassian.jira.plugin.projectpanel.ProjectTabPanelModuleDescriptor;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.browse.BrowseContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.software.api.conditions.ProjectDevToolsIntegrationFeatureCondition;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.FieldVisibilityManager;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Map;

import static com.atlassian.jira.permission.ProjectPermissions.VIEW_DEV_TOOLS;
import static com.atlassian.jira.plugin.ext.bamboo.applinks.BambooApplicationLinkManager.Filter.UNFILTERED;
import static com.atlassian.jira.plugin.ext.bamboo.panel.BuildsForProjectTabPanel.CONTEXT_KEY_CONTEXT_TYPE;
import static com.atlassian.jira.plugin.ext.bamboo.panel.BuildsForProjectTabPanel.CONTEXT_KEY_USER;
import static com.atlassian.jira.plugin.ext.bamboo.panel.BuildsForProjectTabPanel.MODEL_KEY_FIELD_VISIBILITY;
import static com.atlassian.jira.plugin.ext.bamboo.panel.BuildsForProjectTabPanel.MODEL_KEY_PORTLET;
import static com.atlassian.jira.plugin.ext.bamboo.panel.BuildsForProjectTabPanel.PROJECT_CONTEXT_TYPE;
import static com.atlassian.jira.plugin.ext.bamboo.panel.BuildsForProjectTabPanel.VIEW_RESOURCE;
import static com.atlassian.jira.software.api.conditions.ProjectDevToolsIntegrationFeatureCondition.CONTEXT_KEY_PROJECT;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SuppressWarnings("deprecation")
public class BuildsForProjectTabPanelTest {
    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private ApplicationUser user;
    @Mock
    private BambooApplicationLinkManager bambooApplicationLinkManager;
    @Mock
    private BambooPanelHelper bambooPanelHelper;
    @Mock
    private BrowseContext context;
    @Mock
    private FieldVisibilityManager fieldVisibilityManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private Project project;
    @Mock
    private ProjectDevToolsIntegrationFeatureCondition projectDevToolsIntegrationFeatureCondition;
    @Mock
    private ProjectTabPanelModuleDescriptor descriptor;

    @InjectMocks
    private BuildsForProjectTabPanel panel;

    @Before
    public void setUpContext() {
        when(context.getProject()).thenReturn(project);
        when(context.getUser()).thenReturn(user);
    }

    @Before
    public void setUpDescriptor() {
        panel.init(descriptor);
    }

    private void assertPanelVisible(final boolean expectedResult) {
        // Invoke
        final boolean showPanel = panel.showPanel(context);

        // Check
        assertThat(showPanel, is(expectedResult));
    }

    private void setUpBambooAppLinked(final boolean appLinked) {
        when(bambooApplicationLinkManager.hasApplicationLinks(UNFILTERED)).thenReturn(appLinked);
    }

    private void setUpDevToolsIntegrationFeatureCondition(final boolean shouldDisplay) {
        when(projectDevToolsIntegrationFeatureCondition.shouldDisplay(anyMapOf(String.class, Object.class)))
                .thenReturn(shouldDisplay);
    }

    private void setUpDevToolsPermission(final boolean userHasPermission) {
        when(permissionManager.hasPermission(VIEW_DEV_TOOLS, project, user)).thenReturn(userHasPermission);
    }

    @SuppressWarnings("unchecked")
    private void verifyBrowseContextMap() {
        final ArgumentCaptor<Map> contextMapCaptor = ArgumentCaptor.forClass(Map.class);
        verify(projectDevToolsIntegrationFeatureCondition).shouldDisplay(contextMapCaptor.capture());
        final Map<String, Object> actualContextMap = contextMapCaptor.getValue();
        assertThat(actualContextMap, hasEntry(CONTEXT_KEY_CONTEXT_TYPE, (Object) PROJECT_CONTEXT_TYPE));
        assertThat(actualContextMap, hasEntry(CONTEXT_KEY_PROJECT, (Object) project));
        assertThat(actualContextMap, hasEntry(CONTEXT_KEY_USER, (Object) user));
    }

    @Test
    public void shouldNotShowPanelIfBambooIsNotAppLinked() {
        // Set up
        setUpBambooAppLinked(false);
        setUpDevToolsPermission(true);
        setUpDevToolsIntegrationFeatureCondition(true);

        // Invoke and check
        assertPanelVisible(false);
    }

    @Test
    public void shouldNotShowPanelIfUserDoesNotHaveDevToolsPermissionToProject() {
        // Set up
        setUpBambooAppLinked(true);
        setUpDevToolsPermission(false);
        setUpDevToolsIntegrationFeatureCondition(true);

        // Invoke and check
        assertPanelVisible(false);
    }

    @Test
    public void shouldNotShowPanelIfDevToolsIntegrationFeatureConditionIsFalse() {
        // Set up
        setUpBambooAppLinked(true);
        setUpDevToolsPermission(true);
        setUpDevToolsIntegrationFeatureCondition(false);

        // Invoke and check
        assertPanelVisible(false);
        verifyBrowseContextMap();
    }

    @Test
    public void shouldShowPanelIfAllConditionsAreMet() {
        // Set up
        setUpBambooAppLinked(true);
        setUpDevToolsPermission(true);
        setUpDevToolsIntegrationFeatureCondition(true);

        // Invoke and check
        assertPanelVisible(true);
        verifyBrowseContextMap();
    }

    @Test
    public void shouldShowCorrectHtmlFromCorrectViewModel() {
        // Set up
        final String mockedHtml = "theHtml";
        when(descriptor.getHtml(anyString(), anyMapOf(String.class, Object.class))).thenReturn(mockedHtml);

        // Invoke
        final String actualHtml = panel.getHtml(context);

        // Check
        assertThat(actualHtml, is(mockedHtml));
        assertGetHtmlArguments();
    }

    @SuppressWarnings("unchecked")
    private void assertGetHtmlArguments() {
        final ArgumentCaptor<String> resourceNameCaptor = ArgumentCaptor.forClass(String.class);
        final ArgumentCaptor<Map> viewModelCaptor = ArgumentCaptor.forClass(Map.class);

        verify(descriptor).getHtml(resourceNameCaptor.capture(), viewModelCaptor.capture());

        assertThat(resourceNameCaptor.getValue(), is(VIEW_RESOURCE));

        final Map<String, Object> actualViewModel = viewModelCaptor.getValue();
        assertThat(actualViewModel, hasEntry(MODEL_KEY_FIELD_VISIBILITY, (Object) fieldVisibilityManager));
        assertThat(actualViewModel, hasEntry(MODEL_KEY_PORTLET, (Object) panel));
    }
}
